package pl.lukpio.carsharing_auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsharingAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarsharingAuthApplication.class, args);
	}

}
