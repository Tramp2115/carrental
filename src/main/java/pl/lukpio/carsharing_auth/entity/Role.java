package pl.lukpio.carsharing_auth.entity;

public enum Role {
    CLIENT, ADMIN
}
